//---------------------------------------------------------------------------

#pragma hdrstop

#include "Clase.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


void gameBoard::movePicture(int pieceID,int row,int col)
{
   switch(pieceID)
   {            //piesele negre
	   case 0: fChess->blackRook1->Left=postocord(col);
			   fChess->blackRook1->Top=postocord(row);
			   break;
	   case 1: fChess->blackKnight1->Left=postocord(col);
			   fChess->blackKnight1->Top=postocord(row);
			   break;
	   case 2: fChess->blackBishop1->Left=postocord(col);
			   fChess->blackBishop1->Top=postocord(row);
			   break;
	   case 3: fChess->blackQueen->Left=postocord(col);
			   fChess->blackQueen->Top=postocord(row);
			   break;
	   case 4: fChess->blackKing->Left=postocord(col);
			   fChess->blackKing->Top=postocord(row);
			   break;
	   case 5: fChess->blackBishop2->Left=postocord(col);
			   fChess->blackBishop2->Top=postocord(row);
			   break;
	   case 6: fChess->blackKnight2->Left=postocord(col);
			   fChess->blackKnight2->Top=postocord(row);
			   break;
	   case 7: fChess->blackRook2->Left=postocord(col);
			   fChess->blackRook2->Top=postocord(row);
			   break;
	   case 8: fChess->blackPawn1->Left=postocord(col);
			   fChess->blackPawn1->Top=postocord(row);
			   break;
	   case 9: fChess->blackPawn2->Left=postocord(col);
			   fChess->blackPawn2->Top=postocord(row);
			   break;
	   case 10: fChess->blackPawn3->Left=postocord(col);
			   fChess->blackPawn3->Top=postocord(row);
			   break;
	   case 11: fChess->blackPawn4->Left=postocord(col);
			   fChess->blackPawn4->Top=postocord(row);
			   break;
	   case 12: fChess->blackPawn5->Left=postocord(col);
			   fChess->blackPawn5->Top=postocord(row);
			   break;
	   case 13: fChess->blackPawn6->Left=postocord(col);
			   fChess->blackPawn6->Top=postocord(row);
			   break;
	   case 14: fChess->blackPawn7->Left=postocord(col);
			   fChess->blackPawn7->Top=postocord(row);
			   break;
	   case 15: fChess->blackPawn8->Left=postocord(col);
			   fChess->blackPawn8->Top=postocord(row);
			   break;

	   //albele

	   case 16: fChess->whitePawn1->Left=postocord(col);
			   fChess->whitePawn1->Top=postocord(row);
			   break;
	   case 17: fChess->whitePawn2->Left=postocord(col);
			   fChess->whitePawn2->Top=postocord(row);
			   break;
	   case 18: fChess->whitePawn3->Left=postocord(col);
			   fChess->whitePawn3->Top=postocord(row);
			   break;
	   case 19: fChess->whitePawn4->Left=postocord(col);
			   fChess->whitePawn4->Top=postocord(row);
			   break;
	   case 20: fChess->whitePawn5->Left=postocord(col);
			   fChess->whitePawn5->Top=postocord(row);
			   break;
	   case 21: fChess->whitePawn6->Left=postocord(col);
			   fChess->whitePawn6->Top=postocord(row);
			   break;
	   case 22: fChess->whitePawn7->Left=postocord(col);
			   fChess->whitePawn7->Top=postocord(row);
			   break;
	   case 23: fChess->whitePawn8->Left=postocord(col);
			   fChess->whitePawn8->Top=postocord(row);
			   break;

	   case 24: fChess->whiteRook1->Left=postocord(col);
				fChess->whiteRook1->Top=postocord(row);
				break;
	   case 25: fChess->whiteKnight1->Left=postocord(col);
			   fChess->whiteKnight1->Top=postocord(row);
			   break;
	   case 26: fChess->whiteBishop1->Left=postocord(col);
			   fChess->whiteBishop1->Top=postocord(row);
			   break;
	   case 27: fChess->whiteQueen->Left=postocord(col);
			   fChess->whiteQueen->Top=postocord(row);
			   break;
	   case 28: fChess->whiteKing->Left=postocord(col);
			   fChess->whiteKing->Top=postocord(row);
			   break;
	   case 29: fChess->whiteBishop2->Left=postocord(col);
			   fChess->whiteBishop2->Top=postocord(row);
			   break;
	   case 30: fChess->whiteKnight2->Left=postocord(col);
			   fChess->whiteKnight2->Top=postocord(row);
			   break;
	   case 31: fChess->whiteRook2->Left=postocord(col);
			   fChess->whiteRook2->Top=postocord(row);
			   break;


   }
}

void gameBoard::movePiece(int pieceID,int row,int col){
  board_mem[getRow(pieceID)][getCol(pieceID)]=-1;
  board_mem[row][col]=pieceID;
  setRow(pieceID,row);
  setCol(pieceID,col);
  movePicture(pieceID,row,col);

}

