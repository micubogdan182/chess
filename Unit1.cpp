//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Clase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfChess *fChess;



gameBoard board;
//---------------------------------------------------------------------------
__fastcall TfChess::TfChess(TComponent* Owner)
	: TForm(Owner)
{int piesa;//id piesa
for(int i=0;i<2;i++)//rand
for(int j=0;j<8;j++){
pieces[piesa].row=i;
pieces[piesa].col=j;
pieces[piesa].color=0;
piesa++;
}
for(int i=6;i<8;i++)
for(int j=0;j<8;j++){
pieces[piesa].row=i;
pieces[piesa].col=j;
pieces[piesa].color=1;
piesa++;
}
}

int pieceID=-1;

int getCol(int x){
	return pieces[x].col;
}

int getRow(int x){
	return pieces[x].row;
}

void setCol(int x, int y){
	pieces[x].col=y;
}

void setRow(int x,int y){
	pieces[x].row=y;
}

int postocord(int colrow){
	return 52+colrow*36;
}

int cordtopos(int xy){
	return  (xy-48)/36;
}




void __fastcall TfChess::imgBoardMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
if(pieceID!=-1)
	board.movePiece(pieceID,cordtopos(Y),cordtopos(X));
	pieceID=-1;
}
//---------------------------------------------------------------------------


void __fastcall TfChess::pictureMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	   aux=dynamic_cast<TImage *>(Sender);
	   int auxID=board.board_mem[cordtopos(aux->Top)][cordtopos(aux->Left)];
	   if(pieceID!=-1){
	   board.movePiece(pieceID,pieces[auxID].row,pieces[auxID].col);
	   pieceID=-1;}
	   else {
		   if(pieceID==auxID)
		   pieceID=-1;
		   else
            pieceID=auxID;
       }


}
//---------------------------------------------------------------------------

