//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H


//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------

int postocord(int rowcol);
int cordtopos(int xy);
int getCol(int x);
int getRow (int x);
void setCol(int x,int y);
void setRow(int x,int y);



class TfChess : public TForm
{
__published:	// IDE-managed Components
	TImage *imgBoard;
	TImage *whiteRook1;
	TImage *whiteRook2;
	TImage *whiteKnight1;
	TImage *whiteKnight2;
	TImage *whiteBishop1;
	TImage *whiteBishop2;
	TImage *whiteQueen;
	TImage *whiteKing;
	TImage *whitePawn1;
	TImage *whitePawn2;
	TImage *whitePawn3;
	TImage *whitePawn4;
	TImage *whitePawn5;
	TImage *whitePawn6;
	TImage *whitePawn7;
	TImage *whitePawn8;
	TImage *blackRook1;
	TImage *blackKnight1;
	TImage *blackBishop1;
	TImage *blackQueen;
	TImage *blackKing;
	TImage *blackBishop2;
	TImage *blackKnight2;
	TImage *blackRook2;
	TImage *blackPawn1;
	TImage *blackPawn2;
	TImage *blackPawn3;
	TImage *blackPawn4;
	TImage *blackPawn5;
	TImage *blackPawn6;
	TImage *blackPawn7;
	TImage *blackPawn8;
	void __fastcall imgBoardMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall pictureMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);

private:	// User declarations
public:		// User declarations
	TImage *aux;
	__fastcall TfChess(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfChess *fChess;
//---------------------------------------------------------------------------
#endif
